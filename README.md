# try-wws

```bash
mkdir hello-world
cd hello-world
cargo init --bin
```

https://workers.wasmlabs.dev/

## Install Wasm Workers Server

```bash
sudo curl https://raw.githubusercontent.com/vmware-labs/wasm-workers-server/main/install.sh | bash
```