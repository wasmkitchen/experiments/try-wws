use anyhow::Result;
use wasm_workers_rs::{
    worker,
    http::{self, Request, Response},
    Content,
};

#[worker]
fn reply(req: Request<String>) -> Result<Response<Content>> {
    Ok(http::Response::builder()
        .status(200)
        .header("x-generated-by", "wasm-workers-server")
        .body(String::from("💜 Hello wasm! 💜").into())?)
}
